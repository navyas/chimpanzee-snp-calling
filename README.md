# Genetic Diversity in Chimpanzee RNA-seq Datasets #
This repository contains all the code used to analyse the SNPs generated after SNP calling with the GATK best practices pipeline 
(https://gitlab.unimelb.edu.au/bshaban/rnaseqgatk) and to produce figures. Additional code used to produce the panTro5 reference genome used for SNP calling 
is available in the `additional code` folder. 

# Table of Contents #

* Filtering multi-sample VCF output
    + Combining chromsomal multi-sample vcf files
    + Keep SNPs that passed filters, and remove indels 
    + Generate site frequency spectrum (SFS)
    + Remove singletons
* Tree (clustering) analysis
    + Missigness
    + PlINK IBS Clustering
    + Tree construction and analysis
    + Resolving species ambiguities of outlier samples
    + Remove outliers
* Ancestry analysis
    + Convert coordinates of reference genomic SNPs
    + Merge with genomic SNPs
    + Apply missigness filter
    + Principal Component Analysis (PCA)
    + Admixture
* Somalier pairwise relatedness
    + 468 samples
    + 237 samples
    + Subset samples on the basis of somalier relatedness
* Pairwise IBD (Ternary Diagrams)
    + Subset samples
    + PLINK pairwise IBD
* Tools 

## Filtering multi-sample VCF ouput ##

### Combining chromosomal multi-sample vcf files ### 

The output of SNP calling with optimized GATK pipeline was a multi-sample vcf-file for each chrosome. Using BCFtools, we combined these into a single file. 

~~~
bgzip <23 individual vcf files>
bcftools index --tbi <23 individual vcf.gz files>
bcftools concat <23 individual vcf.gz files in order from 1 to 22> > rnaseq-snps.vcf
~~~

The chimpanzee chromosome has a split chromosome 2, referred to as chromosome 2A and 2B. We renamed these to 2 and 23 respectively to make further analysis
easier.

~~~
sed 's/^2A/2/g' rnaseq.vcf | sed 's/^2B/23/g' > rnaseq-renamed.vcf
~~~

### Keep SNPs that passed filters ###

Using vcftools, we only retained SNPs that passed SNP calling filters.

~~~
vcftools --vcf rnaseq-renamed.vcf  --remove-filtered-all --recode --recode-INFO-all --out rnaseq-filtered
~~~

Next, we removed indels. 

~~~
vcftools --vcf rnaseq-filtered.vcf --remove-indels --recode --recode-INFO-all --out rnaseq-snps-filtered
~~~

### Generate site frequency spectrum ###

Using PLINK, we calculate the minor allele frequency of all sites. 

~~~
plink --vcf rnaseq-snps-filtered.vcf --freq
~~~

`SFS.R` uses the .frq output of this to produce a site frequency spectrum.

### Remove singletons ###

We remove singletons from our dataset with VCFtools.

~~~
vcftools --vcf rnaseq-snps-filtered.vcf --singletons rnaseq-snps
vcftools --vcf rnaseq-snps-filtered.vcf --exclude-positions rnaseq-snps.singletons --recode --recode-INFO-all --out rnaseq-snps-filtered-nosingletons
~~~

## Tree (clustering) analysis ##

### Missingness thresholds ###

Missingness thresholds were applied to unmerged RNA-seq dataset, with VCFtools.

~~~
vcftools --vcf rnaseq-snps-filtered-nosingletons --max-missingn 0.95 --recode --recode-INFO-all --out rnaseq-snps-filtered-miss5
~~~

A --max-missing of 0.95 specifies that sites are only allowed to be missing from 5% of samples.

### PLINK IBS Clustering ###

PLINK was used to compute genome-wide IBS pairwise distances between all samples (https://zzz.bwh.harvard.edu/plink/strat.shtml). 

~~~
plink --vcf rnaseq-snps-filtered-miss5.vcf --make-bed --double-id --out rnaseq-snps-filtered-miss5
plink --bfile rnaseq-snps-filtered-miss5 --cluster --out 
~~~

One of the outputs of this is a similarity matrix based on IBS distances. 

### Tree construction and analysis ###

`tree-code.R` uses this output to produce a distance matrix, perform hierarchal clustering and generate trees.

### Resolving species ambiguities of outlier samples ###

Initial tree clustering identified potential outliers. Outlier samples were validated as detailed in `chimp_ambiguous_samples.md`

### Remove outliers ###

A text file list of these outliers was generated and VCFtools was used to remove them from the dataset. 

~~~
vcftools --vcf rnaseq-snps-filtered-nosingletons.vcf --remove outliers.txt --recode --recode-INFO-all --out rnaseq-snps-filtered-nosingletons-nooutliers
~~~

## Ancestry Analysis ##

### Convert coordinates of reference genomic SNPs ###

The coordinates of the SNPs from De Manuel et al. 2016 used in our study were converted from panTro4 to panTro5. The chain file was downloaded from the
UCSC Genome Browser. 

~~~
rsync -avzP rsync://hgdownload.cse.ucsc.edu/goldenPath/panTro4/liftOver/panTro4ToPanTro5.over.chain.gz .
~~~

We used CrossMap to convert the coordinates.

~~~
CrossMap.py vcf <chain file> <input> <reference> <output>
~~~

The output was compressed with BCFtools, sorted and then recompressed and indexed. 
~~~
bgzip <input.vcf> > <output.vcf.gz>
bcftools sort <input.vcf.gz> > <output.vcf>
bgzip  <input.vcf> > <output.vcf.gz>
tabix -p <input.vcf.gz>
~~~

We then removed any unclassified contigs and edited the header to only show contigs present in the clean file. 

~~~
bcftools view <input.vcf.gz> -r 1,2A,2B,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22 > <output.vcf>
awk '!/random/ && !/Un_NW/ && !/ID=M/ && !/ID=X/ && !/ID=Y/' <input.vcf> > <ouput.vcf>
~~~

### Merge with genomic SNPs ###

We used BCFtools to merge the filtered RNA-seq SNPs with genomic SNPs. We compressed and generated index for the filered rna-seq vcf file before merging the
two datasets

~~~
bgzip rnaseq-snps-filtered-nosingletons-nooutliers.vcf 
bcftools index --tbi rnaseq-snps-filtered-nosingletons-nooutliers.vcf.gz
bcftools merge genomic-snps.vcf.gz rnaseq-snps-filtered-nosingletons-nooutliers.vcf.gz > merged-snps.vcf
~~~

### Apply missingness filters ###

VCFtools was use to apply a missingness threshold to the merged dataset.  

~~~
vcftools --vcf merged-snps.vcf --max-missing 0.95 --recode --recode-INFO-all --out merged-snps-missingness5
~~~

We also applied thresholds of 0.90, 0.98 and 0.99.

### Principal Component Analysis (PCA) ###

We used the Eigensoft SmartPCA package to perform a principal component analysis for the 59 WGS chimps. SmartPCA requires plink .bed, .bim and .fam files
for input. First we used VCFtools to keep only genomic samples and then plink to generate the requred file. 


~~~
vcftools --vcf merged-snps-missingness5.vcf --keep genomic-samples-list.txt --out missingness5-genomic-samples
plink --vcf merged-snps-missingness5.vcf --make-bed --double-id --out merged-snps-miss5
~~~

We modified the output .fam file manually, to includy ancestry information in the sixth, phenotype value column
(https://www.cog-genomics.org/plink/1.9/formats#fam). For example: 

~~~
ellioti Taweh 0 0 0 Nigeria-Cameroon
ellioti Tobi 0 0 0 Nigeria-Cameroon
schweinfurthii Vincent 0 0 0 Eastern
schweinfurthii Andromeda 0 0 0 Eastern
~~~

We specified the SmartPca parameters into a parsefile:

~~~
genotypename:   merged-snps-miss5.bed
snpname:        merged-snps-miss5.bim
indivname:	merged-snps-miss5.fam
evecoutname:    merged-snps-miss5.evec
evaloutname:    merged-snps-miss5.eval
numoutlieriter: 0
~~~

and executed smartpca. 

~~~
smartpca -p parsefile.par
~~~

`PCA.R` then uses the eigenvector output of smartpca and produces PC1/PC2 and PC2/PC3 plots. 

We repeated these steps with all the merged samples. This time we added an additional poplistname parameter, which specified a text file with the list of 
populations in the dataset, like such: 

~~~
Central
Western
Eastern
Nigeria-Cameroon
~~~

With this parameter, we projected our samples of unknown ancestry into the space of the genomic samples. `PCA.R` was again use to plot the results. 


### Admixture ### 

ADMIXTURE was used to quantify ancestry proportions in our samples. We used the WGS chimps as a reference to perform supervised admixture. This requires 
the plink bed, bim and fam files and an additional manually created pop file as described in the ADMIXTURE manual.

~~~
admixture merged-snps-miss5.bed 4 --supervised 
~~~

`Admixture.R` uses the results of the output .Q (ancestry fractions) results to produce a bar graph showing ancestry fractions for each sample. Samples have 
been ordered and seperated by the study from which they were taken. 

## Somalier pairwise relatedness ##

### 468 samples ###

Missigness of 5% and minor allele frequency of 0.25 was applied to the unmerged samples (without outliers).The output was compressed with index. 

~~~
vcftools --vcf rnaseq-snps-filtered-nosingletons-nooutliers.vcf --max-missing 0.95 --maf 0.25 --recode --recode-INFO-all --out rnaseq-snps-nooutliers-miss5-maf25
bgzip  rnaseq-snps-nooutliers-miss5-maf25.vcf
bcftools index --tbi rnaseq-snps-nooutliers-miss5-maf25.vcf.gz
~~~

We then executed Somalier. 

~~~
somalier extract -d extracted/ --sites rnaseq-snps-nooutliers-miss5-maf25.vcf.gz -f <path to reference> rnaseq-snps-nooutliers-miss5-maf25.vcf.gz
somalier relate extracted/*.somalier
~~~

The somalier.pairs.tsv output was then analysed by `somalier.R` and used to produce plots. 

### 237 samples ###

A list of 237 samples to subset, excluding within study replicates, was generated (see below). These samples were retained using VCFtools.

~~~
vcftools --vcf rnaseq-snps-nooutliers-miss5-maf25.vcf --keep samples_237.txt --recode --recode-INFO-all --out rnaseq-snps-nooutliers-miss5-maf25-237samples
~~~

The vcf was then compressed and somalier was executed and analysed as above. 

### Subset samples on the basis of somalier relatedness ###

To filter out replicate  or duplicate samples from clusters on the basis of somalier relatedness, we executed `get_samples.R`. 


## Pairwise IBD (Ternary Diagrams) ##

### Subset samples ###

Text file lists of names of 135 "unique" samples and samples from different locations were created and these samples were in turn subsetted using vcftools. 

~~~
vcftools --vcf rnaseq-snps-nooutliers-miss5-maf25.vcf --keep samples-relevant.txt --recode --recode-INFO-all --out 
~~~

### PlINK pairwise IBD

PLINK was used to get pairwise IBD estimates for all pairs of samples. 

~~~
plink --vcf samples-relevant.vcf --make-bed --double-id --out samples-relevant
plink --bfile samples-relevant --genome --out samples-relevant
~~~

`ternaries.R` used the output .genome file to analyse and make ternary plots. 

## Tools ##

We used the following tools in our analysis:
- VCFtools (v.0.1.15)
- BCFtools (v.1.9)
- PLINK (v.1.9.0)
- Kallisto (v.0.4.5.1) 
- CrossMap (v.0.3.8)
- Eigensoft SmartPCA (v.7.2.1)
- ADMIXTURE (v.1.3.0)
- Somalier( v.0.2.10)
- ape R package (v.5.4-1)
- treeio R package (v.2.2.4 )
- ggtern R package (v.3.3.0)






