###Part of a set of scripts for analysing diversity in chimpanzees using SNPs called from public RNA-seq data. 

###This is part of a set of scripts that looks at relatedness between samples.This script uses the genome files from the plink 
###pairwise IBD estimation to produce ternary diagrams based on the Z0, Z1, Z2 statistics, as described by  Blay et al. 2019. 
###The code has been adapted from Galvan-Fermenia et al, 2017, "Graphics for relatedness research".  

###NS, 25.06.21

###CONTENTS
###0. LOAD PACKAGES
###1. TERNARY FOR SAMPLES FROM FAIR ET AL,2020
###2. TERNARY FOR SAMPLES FROM YERKES
###3. TERNARY FOR ALL 135 SAMPLES
###4. TERNARIES FOR SAMPLES FROM BZ, GW AND SNPRC
###5. LEGEND PLOT 
###6. MULTIPLOT FUNCTION FOR MULTIPANEL GGTERN OBJECTS (Galvan-Fermenia et al, 2017)
###7. TERNARIES PANEL



########################################################################
###0. LOAD PACKAGES
########################################################################

library(ggtern)
library(ggplot2)
library(cowplot)
library(ggpubr)



########################################################################
###1. TERNARY FOR SAMPLES FROM FAIR ET AL,2020
########################################################################

#read known pedigree information from yerkes centre, column 1 and column 2 are the related pair and column 3 is their relatedness
ped <- read.table("yerkes_pedigree2.txt")

#create data frame with  yerkes ids and corresponding sample ids from our metadata
s31_ids <- c(295, 317, 338, 389, 438, 456, 462, 476, 495, 503, 522, 529, 537, 549, 554, 558, 570, 623, 676, 724)
s31_names <- c(411, 412, 413, 414, 415, 416, 417, 418, 419, 433, 434, 435, 436, 437, 438, 440, 441,442, 443, 444)
s31_data <- data.frame(names = s31_names, ids = s31_ids)

#subset from pedigree only pairs found in study 31
ped_31<- ped[ped$V2 %in% s31_ids,]
ped_31 <- ped_31[ped_31$V1 %in% s31_ids,]

#this loop changes the yerkes ids in ped_31 to the corresponding sample ids from our metadata
for(i in seq(20)) {
  ped_31 <- data.frame(lapply(ped_31, function(x) gsub(s31_data$ids[i], s31_data$names[i], x)))
}

#removes duplicate pairs from the pedigree
ped_31 <- ped_31[ped_31$V2 > ped_31$V1,]

#load genome data for s31
genome <- read.table("s31-maf25-noindels.genome", head = TRUE)
genome$relation <- rep("no known relation", nrow(genome))

#thresholds for inferring relationships, chosen from the results of this ternary and retroactively applied. 
genome$relation[genome$Z1 >= 0.70] <- "inferred parent/offspring"
genome$relation[genome$Z1 < 0.7 & genome$Z1 > 0.40] <- "inferred 2nd degree"
genome$relation[genome$Z2 >= 0.65 & genome$Z0 < 0.20] <- "inferred identical"


#marking known relationships using pedigree dataframe 
for (i in seq(nrow(ped_31)))
{
  
  if(ped_31$V3[i] == 0.25){
    
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_31$V1[i] & genome$IID2 == ped_31$V2[i],]))] <- "known parent/offspring"
  }
  if(ped_31$V3[i] == 0.125)
  {
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_31$V1[i] & genome$IID2 == ped_31$V2[i],]))] <- "known 2nd degree"
    
  }
  
  if(ped_31$V3[i] == 0.0625 )
  {
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_31$V1[i] & genome$IID2 == ped_31$V2[i],]))] <- "known 3rd degree"
    
  }
}

#ternary
s31_ternary <-  ggtern(data = genome, aes(Z0, Z1, Z2)) +
  geom_point(aes(shape = relation),size = 2.5, alpha = 0.75) +
  scale_shape_manual(values = c(16, 1, 17, 2, 18, 0, 4), breaks = c("known parent/offspring", "inferred parent/offspring", "known 2nd degree", "inferred 2nd degree",  "known 3rd degree", "inferred identical", "no known relation"), "Relation") + 
  theme_bw() + labs(title="C. Study 31") + geom_Tline(Tintercept = 0.7) +  geom_Rline(Rintercept = 0.70) + geom_Rline(Rintercept = 0.30) + geom_Tline(Tintercept = 0.40) + theme(legend.position = "none")



########################################################################
###2. TERNARY FOR SAMPLES FROM YERKES
########################################################################

#make dataframe of yerkes samples in our dataset, their yerkes id and taxa names
other_ids <- c(597, 668, 636, 750, 726, 575, 573, 471, 728, 643, 748, 525)
s31_ids <- c(295, 317, 338, 389, 438, 456, 462, 476, 495, 503, 522, 529, 537, 549, 554, 558, 570, 623, 676)
combined <- c(other_ids, s31_ids)
combined_names <- c(527, 521, 534, 541, 530, 554, 522, 246, 198, 212, 511,553 , 
                    606, 412, 580, 414, 587, 578, 588, 572, 419, 433, 434, 435, 436, 595, 438, 440, 590,442, 443)
combined_data <- data.frame(names = combined_names, ids = combined)


#subset from the yerkes pedigree pairings that are in our dataset
ped_yerkes <- ped[ped$V2 %in% combined,]
ped_yerkes <- ped_yerkes[ped_yerkes$V1 %in% combined,]


#substitute yerkes sample ids for sample ids used in our dataset
for(i in seq(31)) {
  ped_yerkes <- data.frame(lapply(ped_yerkes, function(x) gsub(combined_data$ids[i], combined_data$names[i], x)))
}

#removes duplicate pairs from the pedigree
ped_yerkes <- ped_yerkes[ped_yerkes$V2 > ped_yerkes$V1,]


#load yerkes genome data
genome <- read.table("yerkes-maf25.genome", head = TRUE)
genome$relation <- rep("no known relation", nrow(genome))

#apply thresholds for inferring relationships
genome$relation[genome$Z1 >= 0.70] <- "inferred parent/offspring"
genome$relation[genome$Z1 < 0.7 & genome$Z1 > 0.40] <- "inferred 2nd degree"
genome$relation[genome$Z2 >= 0.65 & genome$Z0 < 0.20] <- "inferred identical"


#marking known relationships using pedigree dataframe 
for (i in seq(nrow(ped_yerkes)))
{
  
  if(ped_yerkes$V3[i] == 0.25){
   
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_yerkes$V1[i] & genome$IID2 == ped_yerkes$V2[i],]))] <- "known parent/offspring"
  }
   if(ped_yerkes$V3[i] == 0.125)
   {
     genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_yerkes$V1[i] & genome$IID2 == ped_yerkes$V2[i],]))] <- "known 2nd degree"
  
   }
  
   if(ped_yerkes$V3[i] == 0.0625 )
   {
     genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_yerkes$V1[i] & genome$IID2 == ped_yerkes$V2[i],]))] <- "known 3rd degree"
  
    }
}

#read plink .genome information for yerkes samples 

genome <- read.table("yerkes-maf25.genome", head = TRUE)
genome$relation <- rep("no known relation", nrow(genome))

#thresholds for inferred relationships
genome$relation[genome$Z1 >= 0.70] <- "inferred parent/offspring"
genome$relation[genome$Z1 < 0.7 & genome$Z1 > 0.40 & genome$Z2 < 0.30] <- "inferred 2nd degree"
genome$relation[genome$Z1 < 0.40 & genome$Z2 > 0.30 & genome$Z0 < 0.40] <- "inferred siblings"
genome$relation[genome$Z2 >= 0.65 & genome$Z0 < 0.20] <- "inferred identical"


#marking known relationships using pedigree dataframe 
for (i in seq(nrow(ped_remaining)))
{
  
  if(ped_remaining$V3[i] == 0.25){
   
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_remaining$V1[i] & genome$IID2 == ped_remaining$V2[i],]))] <- "known parent/offspring"
  }
   if(ped_remaining$V3[i] == 0.125)
   {
     genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_remaining$V1[i] & genome$IID2 == ped_remaining$V2[i],]))] <- "known 2nd degree"
  
   }
  
   if(ped_remaining$V3[i] == 0.0625 )
   {
     genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_remaining$V1[i] & genome$IID2 == ped_remaining$V2[i],]))] <- "known 3rd degree"
  
    }
}

##shorten relationships into broad categories for indicating colour
genome$relation_short <- ("no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred parent/offspring" | genome$relation == "known parent/offspring", "parent", "no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred 2nd degree" | genome$relation == "known 2nd degree", "second", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "known 3rd degree", "third", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred identical", "identical", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred siblings", "siblings", genome$relation_short)


yerkes_ternary <- ggtern(data = genome, aes(Z0, Z1, Z2)) + 
  scale_colour_manual(values = c( "black","dodgerblue4", "darkgoldenrod1", "violetred1", "seagreen1", "firebrick"), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  scale_alpha_manual(values = c(0.35, 0.75,0.75, 0.75, 0.75,0.75), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  geom_point(aes(shape = relation, color = relation_short, alpha = relation_short),size = 0.7) +
  scale_shape_manual(values = c(16, 1, 17, 2, 18, 0, 4,4), breaks = c("known parent/offspring", "inferred parent/offspring", "known 2nd degree", "inferred 2nd degree",  "known 3rd degree", "inferred identical", "inferred siblings", "no known relation"), "Relation") + 
  theme_bw() + labs(title="C. Yerkes") + geom_Tline(Tintercept = 0.7) +  geom_Rline(Rintercept = 0.65) + geom_Rline(Rintercept = 0.30) + geom_Tline(Tintercept = 0.40) + theme(legend.position = "none") 


yerkes_ternary <- yerkes_ternary + theme(text = element_text(size = 7))

########################################################################
###3. TERNARY FOR ALL 135 SAMPLES
########################################################################

#read the genome file for the 135 unique samples
genome <- read.table("chimps-135-maf25.genome", head = TRUE)
genome$relation <- rep("no known relation", nrow(genome))

#thresholds for inferred relationships
genome$relation[genome$Z1 >= 0.70] <- "inferred parent/offspring"
genome$relation[genome$Z1 < 0.7 & genome$Z1 > 0.40 & genome$Z2 < 0.30] <- "inferred 2nd degree"
genome$relation[genome$Z2 > 0.30 & genome$Z0 < 0.40] <- "inferred siblings"
genome$relation[genome$Z2 > 0.30 & genome$Z0 < 0.40 & genome$Z1 > 0.30] <- "inferred siblings"
genome$relation[genome$Z2 >= 0.65 & genome$Z0 < 0.20] <- "inferred identical"


#marking known relationships from pedigree information
for (i in seq(nrow(ped_remaining)))
{
  
  if(ped_remaining$V3[i] == 0.25){
    
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_remaining$V1[i] & genome$IID2 == ped_remaining$V2[i],]))] <- "known parent/offspring"
  }
  if(ped_remaining$V3[i] == 0.125)
  {
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_remaining$V1[i] & genome$IID2 == ped_remaining$V2[i],]))] <- "known 2nd degree"
    
  }
  
  if(ped_remaining$V3[i] == 0.0625 )
  {
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_remaining$V1[i] & genome$IID2 == ped_remaining$V2[i],]))] <- "known 3rd degree"
    
  }
}


##shorten relationships into broad categories for indicating colour
genome$relation_short <- ("no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred parent/offspring" | genome$relation == "known parent/offspring", "parent", "no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred 2nd degree" | genome$relation == "known 2nd degree", "second", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "known 3rd degree", "third", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred identical", "identical", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred siblings", "siblings", genome$relation_short)


all_ternary <- ggtern(data = genome, aes(Z0, Z1, Z2)) + 
  scale_colour_manual(values = c( "black","dodgerblue4", "darkgoldenrod1", "violetred1", "seagreen1", "firebrick"), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  scale_alpha_manual(values = c(0.35, 0.75,0.75, 0.75, 0.75,0.75), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  geom_point(aes(shape = relation, color = relation_short, alpha = relation_short),size = 0.7) +
  scale_shape_manual(values = c(16, 1, 17, 2, 18, 0, 4,4), breaks = c("known parent/offspring", "inferred parent/offspring", "known 2nd degree", "inferred 2nd degree",  "known 3rd degree", "inferred identical", "inferred siblings", "no known relation"), "Relation") + 
  theme_bw() + labs(title="B. 135 'unique' individuals") + geom_Tline(Tintercept = 0.7) +  geom_Rline(Rintercept = 0.65) + geom_Rline(Rintercept = 0.30) + geom_Tline(Tintercept = 0.40) + theme(legend.position = "none") 


all_ternary <- all_ternary + theme(text = element_text(size = 7))


########################################################################
###4. TERNARIES FOR SAMPLES FROM BZ, GW AND SNPRC
########################################################################

##bz

genome <- read.table("bz-maf25.genome", head = TRUE)
genome$relation <- rep("no known relation", nrow(genome))

genome$relation[genome$Z1 >= 0.70] <- "inferred parent/offspring"
genome$relation[genome$Z1 < 0.7 & genome$Z1 > 0.40 & genome$Z2 < 0.30] <- "inferred 2nd degree"
genome$relation[genome$Z1 < 0.40 & genome$Z2 > 0.30 & genome$Z0 < 0.40] <- "inferred siblings"
genome$relation[genome$Z2 >= 0.65 & genome$Z0 < 0.20] <- "inferred identical"


genome$relation_short <- ("no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred parent/offspring" | genome$relation == "known parent/offspring", "parent", "no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred 2nd degree" | genome$relation == "known 2nd degree", "second", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "known 3rd degree", "third", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred identical", "identical", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred siblings", "siblings", genome$relation_short)

bz_ternary <- ggtern(data = genome, aes(Z0, Z1, Z2)) + 
  scale_colour_manual(values = c( "black","dodgerblue4", "darkgoldenrod1", "violetred1", "seagreen1", "firebrick"), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  scale_alpha_manual(values = c(0.35, 0.75,0.75, 0.75, 0.75,0.75), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  geom_point(aes(shape = relation, color = relation_short, alpha = relation_short),size = 0.7) +
  scale_shape_manual(values = c(16, 1, 17, 2, 18, 0, 4,4), breaks = c("known parent/offspring", "inferred parent/offspring", "known 2nd degree", "inferred 2nd degree",  "known 3rd degree", "inferred identical", "inferred siblings", "no known relation"), "Relation") + 
  theme_bw() + labs(title="F. BZ") + geom_Tline(Tintercept = 0.7) +  geom_Rline(Rintercept = 0.65) + geom_Rline(Rintercept = 0.30) + geom_Tline(Tintercept = 0.40) + theme(legend.position = "none") 


bz_ternary <- bz_ternary + theme(text = element_text(size = 7))

##gw

genome <- read.table("gw-maf25.genome", head = TRUE)
genome$relation <- rep("no known relation", nrow(genome))

genome$relation[genome$Z1 >= 0.70] <- "inferred parent/offspring"
genome$relation[genome$Z1 < 0.7 & genome$Z1 > 0.40 & genome$Z2 < 0.30] <- "inferred 2nd degree"
genome$relation[genome$Z1 < 0.40 & genome$Z2 > 0.30 & genome$Z0 < 0.40] <- "inferred siblings"
genome$relation[genome$Z2 >= 0.65 & genome$Z0 < 0.20] <- "inferred identical"

genome$relation_short <- ("no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred parent/offspring" | genome$relation == "known parent/offspring", "parent", "no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred 2nd degree" | genome$relation == "known 2nd degree", "second", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "known 3rd degree", "third", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred identical", "identical", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred siblings", "siblings", genome$relation_short)

gw_ternary <- ggtern(data = genome, aes(Z0, Z1, Z2)) + 
  scale_colour_manual(values = c( "black","dodgerblue4", "darkgoldenrod1", "violetred1", "seagreen1", "firebrick"), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  scale_alpha_manual(values = c(0.35, 0.75,0.75, 0.75, 0.75,0.75), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  geom_point(aes(shape = relation, color = relation_short, alpha = relation_short),size = 0.7) +
  scale_shape_manual(values = c(16, 1, 17, 2, 18, 0, 4,4), breaks = c("known parent/offspring", "inferred parent/offspring", "known 2nd degree", "inferred 2nd degree",  "known 3rd degree", "inferred identical", "inferred siblings", "no known relation"), "Relation") + 
  theme_bw() + labs(title="E. GW") + geom_Tline(Tintercept = 0.7) +  geom_Rline(Rintercept = 0.65) + geom_Rline(Rintercept = 0.30) + geom_Tline(Tintercept = 0.40) + theme(legend.position = "none") 


gw_ternary <- gw_ternary + theme(text = element_text(size = 7))

#snprc

genome <- read.table("snprc-maf25.genome", head = TRUE)
genome$relation <- rep("no known relation", nrow(genome))

genome$relation[genome$Z1 >= 0.70] <- "inferred parent/offspring"
genome$relation[genome$Z1 < 0.7 & genome$Z1 > 0.40 & genome$Z2 < 0.30] <- "inferred 2nd degree"
genome$relation[genome$Z1 < 0.40 & genome$Z2 > 0.30 & genome$Z0 < 0.40] <- "inferred siblings"
genome$relation[genome$Z2 >= 0.65 & genome$Z0 < 0.20] <- "inferred identical"


genome$relation_short <- ("no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred parent/offspring" | genome$relation == "known parent/offspring", "parent", "no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred 2nd degree" | genome$relation == "known 2nd degree", "second", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "known 3rd degree", "third", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred identical", "identical", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred siblings", "siblings", genome$relation_short)

snprc_ternary <- ggtern(data = genome, aes(Z0, Z1, Z2)) + 
  scale_colour_manual(values = c( "black","dodgerblue4", "darkgoldenrod1", "violetred1", "seagreen1", "firebrick"), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  scale_alpha_manual(values = c(0.35, 0.75,0.75, 0.75, 0.75,0.75), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  geom_point(aes(shape = relation, color = relation_short, alpha = relation_short),size = 0.7) +
  scale_shape_manual(values = c(16, 1, 17, 2, 18, 0, 4,4), breaks = c("known parent/offspring", "inferred parent/offspring", "known 2nd degree", "inferred 2nd degree",  "known 3rd degree", "inferred identical", "inferred siblings", "no known relation"), "Relation") + 
  theme_bw() + labs(title="D. SNPRC") + geom_Tline(Tintercept = 0.7) +  geom_Rline(Rintercept = 0.65) + geom_Rline(Rintercept = 0.30) + geom_Tline(Tintercept = 0.40) + theme(legend.position = "none") 

snprc_ternary <- snprc_ternary + theme(text = element_text(size = 7))


########################################################################
###5. LEGEND PLOT 
########################################################################

legend_data = data.frame(k0 = c(0,0.25,0.5,0.75,1,0.75,0.50),k1=c(1,0.5,0.5,0.25,0,0,0),k2=c(0,0.25,0,0,0,0,0))
leg_lab = c("known parent/offspring", "inferred parent/offspring", "known 2nd degree", "inferred 2nd degree",  "known 3rd degree", "inferred identical", "no known relation") 
legend_data$family <- leg_lab

plegend = ggplot(legend_data,aes(x=legend_data[,1]+200,y=legend_data[,3]+200,xend=1,yend=1,
                                 shape=legend_data[,4], 
                                 group=legend_data[,4])) +
  geom_point(size=4)+ geom_blank()+
  xlab("") + 
  ylab("") +
  theme_bw() + theme(axis.ticks = element_blank()
                     , legend.position = c(.3,.5), legend.text=element_text(size=10),
                     legend.title=element_text(size=12,face = "bold"),
                     ,panel.grid.major = element_blank()
                     ,panel.grid.minor = element_blank()
                     ,panel.border = element_blank()
                     ,axis.line=element_blank(), axis.text = element_blank()) +
  scale_colour_manual(values="black")+
  scale_shape_manual(name = "Family relationships",
                     values = c(16, 1, 17, 2, 18, 0, 4),
                     labels = leg_lab)+
  scale_x_continuous(limits = c(0, 1))+
  scale_y_continuous(limits = c(0, 1))

plegend <- plegend + theme(legend.text  = element_text(size = 7))
plegend <- plegend + theme(legend.title   = element_text(size = 7))



####################################################################################
###6. MULTIPLOT FUNCTION FOR MULTIPANEL GGTERN OBJECTS (Galvan-Fermenia et al, 2017)
####################################################################################

 multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  library(grid)
  
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}


########################################################################
###7. TERNARIES PANEL
########################################################################

plots = list()

plots[[1]] = s31_ternary
plots[[2]] = all_ternary
plots[[3]] = yerkes_ternary
plots[[4]] = snprc_ternary
plots[[5]] = gw_ternary
plots[[6]] = bz_ternary

lay_vec =   c(1,2,3,4,5,6)

layout <- matrix(lay_vec, nrow = 2,ncol=3, byrow = TRUE)

pdf("ternaries_final.pdf",width = 6.8,height = 3)
multiplot(plotlist = plots, layout = layout)
plegend
dev.off()

########################################################################
###8. SUPPLEMENTARY TERNARY 
########################################################################

#read genome file with all 300 possible pairs between 25 samples
genome <- read.table("dubious-maf25.genome", head = TRUE)
genome$relation <- rep("no known relation", nrow(genome))

#thresholds for inferred relationships
genome$relation[genome$Z1 >= 0.70] <- "inferred parent/offspring"
genome$relation[genome$Z1 < 0.7 & genome$Z1 > 0.40 & genome$Z2 < 0.30] <- "inferred 2nd degree"
genome$relation[genome$Z1 < 0.40 & genome$Z2 > 0.30 & genome$Z0 < 0.40] <- "inferred siblings"
genome$relation[genome$Z2 >= 0.65 & genome$Z0 < 0.20] <- "inferred identical"


#mark known relationships from pedigree information
for (i in seq(nrow(ped_remaining)))
{
  
  if(ped_remaining$V3[i] == 0.25){
    
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_remaining$V1[i] & genome$IID2 == ped_remaining$V2[i],]))] <- "known parent/offspring"
  }
  if(ped_remaining$V3[i] == 0.125)
  {
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_remaining$V1[i] & genome$IID2 == ped_remaining$V2[i],]))] <- "known 2nd degree"
    
  }
  
  if(ped_remaining$V3[i] == 0.0625 )
  {
    genome$relation[as.numeric(rownames(genome[genome$IID1 == ped_remaining$V1[i] & genome$IID2 == ped_remaining$V2[i],]))] <- "known 3rd degree"
    
  }
}

##mark sample pairs known to be identical 
genome$relation[41] = "known identical"
genome$relation[106] = "known identical"
genome$relation[142] = "known identical"
genome$relation[186] = "known identical"
genome$relation[201] = "known identical"
genome$relation[210] = "known identical"
genome$relation[235] = "known identical"
genome$relation[6] = "known identical"
genome$relation[19] = "known identical"



genome$relation_short <- ("no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred parent/offspring" | genome$relation == "known parent/offspring", "parent", "no known relation")
genome$relation_short <- ifelse(genome$relation == "inferred 2nd degree" | genome$relation == "known 2nd degree", "second", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "known 3rd degree", "third", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred identical" | genome$relation == "known identical", "identical", genome$relation_short)
genome$relation_short <- ifelse(genome$relation == "inferred siblings", "siblings", genome$relation_short)


#indicate cryptic pairs from supplementary table 4
pairs_dubious <- read.table("supplementary_table_4.tsv", fill=T, sep="\t", header=T)

genome$dubious <- rep("no", 300)

for(i in 1:18)
{
  c <- pairs_dubious$sample_a[i]
  d <- pairs_dubious$sample_b[i]
  
  for(j in 1:300)  {
    a <- paste("Sample_",genome$IID1[j],sep = "")
    b <- paste("Sample_",genome$IID2[j],sep = "")
    
    if(a == c & b == d)
      
    {
      genome$dubious[j] <- "yes"
    }
  }
  
}

genome$relation[genome$dubious == "yes"] <- "dubious"



dubious_ternary <- ggtern(data = genome, aes(Z0, Z1, Z2)) + 
  scale_colour_manual(values = c( "black","dodgerblue4", "darkgoldenrod1", "violetred1", "seagreen1", "firebrick"), breaks = c("no known relation", "second", "third", "parent", "identical", "siblings")) + 
  scale_alpha_manual(values = c(0.35, 0.75,0.75, 0.75, 0.75,0.75), breaks = c("no known relation", "second", "third", "parent", "identical","siblings")) + 
  geom_point(aes(shape = relation, color = relation_short, alpha = relation_short),size = 0.7) +
  scale_shape_manual(values = c(16, 1, 17, 2, 18, 0, 4,15, 8, 4), breaks = c("known parent/offspring", "inferred parent/offspring", "known 2nd degree", "inferred 2nd degree",  "known 3rd degree", "inferred identical", "no known relation", "known identical", "dubious", "inferred siblings"), "Relation") + 
  theme_bw() + labs(title="") + geom_Tline(Tintercept = 0.7) +  geom_Rline(Rintercept = 0.65) + geom_Rline(Rintercept = 0.30) + geom_Tline(Tintercept = 0.40) + theme(legend.position = "none") 
dubious_ternary <- dubious_ternary + theme(text = element_text(size = 7))

dubious_ternary



##supplementary ternary panel 


pdf("supplementary_ternary.pdf", width = 6.8, height = 3)
dubious_ternary
dev.off()

