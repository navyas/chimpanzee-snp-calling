--- 
title: Resolving species ambiguities
author: "Irene Gallego Romero"
date: "29 March 2021"
output: html_document
---

1. Make custom reference genome by concatenating rCRS (NC_012920.1) and the chimp equivalent (NC_001643.1), which is an old Clint sequence from panTro3. These are combined together into a single `mtdna_genomes.fa`. Because ERR247263 below could potentially be an orang-utan or a macaque, I'm also adding those two genomes, respectively NC_002083.1 and NC_005943.1.

2. Grab samples from sra using `fastq-dump` - I grab the 18 suspect files as well as a couple of decent ones that we are confident ARE chimpanzees. (The last two rows are the known normal samples)

NB: Rows 21-30 are 8 more day 15 cardiomyocyte samples (diff animals), and 2 treated cardiomyocytes samples (same animal) from the same paper as 17-18 (Bryan's chimp hypoxia paper) that I added later, just for sanity checking that the 20% human reads are not simply from tissue differences with my reference good chimps. 

```
samples sraID paired
222 ERR247263 1
466 SRR11034017 1
468 SRR11034020 1
470 SRR11034023 1
472 SRR11034025 1
473 SRR11034026 1
474 SRR11034027 1
475 SRR11034028 1
476 SRR11034029 1
478 SRR11034031 1
479 SRR11034032 1
481 SRR11034034 1
482 SRR11034035 1
484 SRR11034037 1
485 SRR11034038 1
488 SRR11034041 1
525 SRR6706651 0
529 SRR6706655 0
136 SRR4418037 0
659 SRR12415977 0
547 SRR6706697 0
552 SRR6706702 0
510 SRR6706626 0
511 SRR6706627 0
512 SRR6706628 0
513 SRR6706629 0
514 SRR6706630 0
515 SRR6706631 0
516 SRR6706632 0
517 SRR6706633 0
```

Grab from SRA:

```bash
  #!/bin/bash
  # Array set up:
  #SBATCH --array=1-30

  # The maximum running time of the job in days-hours:mins:sec
  #SBATCH --time=05:30:00

  # The name of the job:
  #SBATCH --job-name="chimpRNA"

  # Output control:
  #SBATCH --output="chimpRNA_%a.log"

  # Do things:
  module load web_proxy
  module load sra-toolkit/2.9.6-1-centos_linux64

  sra=`head -n $SLURM_ARRAY_TASK_ID /scratch/punim0586/igallego/chimpAssignments/samples_to_check.txt | tail -n 1 | cut -f2 -d ' '`
  fastq-dump --gzip --split-files --outdir /scratch/punim0586/igallego/chimpAssignments $sra; 
```

3. Map using kallisto, which necesitates different handling of paired and single-end files; 222 and the humanzee ones (400s) are paired (1 in column 3), the others are single (0 in column 3). First we make the index:

```bash
  module load kallisto/0.45.1 
  kallisto index -i mtdna_index mtdna_genomes.fa
```

Then we set up to process paired and single-end separately:

```bash
  #!/bin/bash
  # Array set up:
  #SBATCH --array=1-30

  # The maximum running time of the job in days-hours:mins:sec
  #SBATCH --time=00:30:00

  # The name of the job:
  #SBATCH --job-name="chimp_map"

  # Output control:
  #SBATCH --output="chimp_map_%a.log"

  # Do things:
  module load kallisto/0.45.1

  sra=`head -n $SLURM_ARRAY_TASK_ID /scratch/punim0586/igallego/chimpAssignments/samples_to_check.txt | tail -n 1 | cut -f2 -d ' '`
  paired=`head -n $SLURM_ARRAY_TASK_ID /scratch/punim0586/igallego/chimpAssignments/samples_to_check.txt | tail -n 1 | cut -f3 -d ' '`

  if [[ "$paired" == "1" ]]
  then 
      eval "kallisto quant -i /scratch/punim0586/igallego/chimpAssignments/mtdna_index -o /scratch/punim0586/igallego/chimpAssignments/${sra}_mapped /scratch/punim0586/igallego/chimpAssignments/${sra}_1.fastq.gz /scratch/punim0586/igallego/chimpAssignments/${sra}_2.fastq.gz 
  "
  else
      eval "kallisto quant -i /scratch/punim0586/igallego/chimpAssignments/mtdna_index -o /scratch/punim0586/igallego/chimpAssignments/${sra}_mapped --single -l 200 -s 20 /scratch/punim0586/igallego/chimpAssignments/${sra}_1.fastq.gz"
  fi
```

Finally, make a little table with the results, which will become supp table whatever:

```bash
  touch allSamplesMapStats.txt

  for i in {1..30} ; do
      sra=`head -n $i /scratch/punim0586/igallego/chimpAssignments/samples_to_check.txt | tail -n 1 | cut -f2 -d ' '`
      human=`head -n 2 ${sra}_mapped/abundance.tsv | tail -n 1`
      chimp=`head -n 3 ${sra}_mapped/abundance.tsv | tail -n 1`
      orang=`head -n 4 ${sra}_mapped/abundance.tsv | tail -n 1`
      rhesus=`head -n 5 ${sra}_mapped/abundance.tsv | tail -n 1`


      echo $sra $human $chimp $orang $rhesus >> allSamplesMapStats.txt
  done
```